<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">    
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="/css/bootstrap.min.css" >
        
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        
        <script src="/js/jquery-3.3.1.min.js" type="text/javascript"></script>
        <script src="/js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="/js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="/js/additional-methods.min.js"></script>
        <script type="text/javascript" src="/js/localization/messages_pt_BR.js"></script>
        <script type="text/javascript" src="/js/jquery.maskedinput.js"></script>

        <script type="text/javascript">                

            jQuery.validator.addMethod("arrobaNoInicio", function(value, element){
                if (value.indexOf("@") == 0){
                    return true
                }else{
                    return false
                }
            }, "Tem que ter arroba no começo")
            

            $(document).ready(function(){
                $("#formCadastro").validate({
                    rules:{
                        nome: {
                            required: true,
                            maxlength: 100,
                            minlength: 10,
                            minWords: 2,
                            style: 'color:#F00'
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        cpf: {
                            required: true
                        },
                        dataNascimento: {
                            required: true
                            
                        },
                    }
                })
            })

        jQuery(function($){
            $("#dataNascimento").mask("99/99/9999")
            $("#cpf").mask("999.999.999-99")
            $("#ddd").mask("(99)")
            $("#telefone").mask("9999-9999")
            $("#telefone").blur(function(event) {
                
                if($(this).val().length == 9){
                  $('.maskFone').mask('99999-9999')
                } else {
                  $('.maskFone').mask('9999-9999')
                }
                $(".maskFone").unmask()
            });
        });
        </script>


        <title>Agenda</title>

    </head>

    <body>
        <div class="container" >
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
                <a class="navbar-brand" href="#">Agenda</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Pessoas </a> 
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="/pessoas"> Listagem</a>
                            <a class="dropdown-item" href="/pessoas/create"> Cadastrar</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
            
        <div class="container" >
            <!-- @yield('navegacao') -->

            @yield('content')

            <!-- @yield('rodape') -->
        </div>

    </body>
</html>