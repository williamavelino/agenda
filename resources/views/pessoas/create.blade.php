@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><i class="fa-user"></i> Cadastro de Pessoa</h2>
                </div>

                <div class="panel-body">

                    <form name="formCadastro" id="formCadastro" method="POST" action="{{ URL::to('pessoas') }}" role="form" class="validate">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Nome</label>
                                        <input autofocus tabindex="1" type="text" class="form-control" id="nome" name="nome" placeholder="" data-validate="required" data-message-required="Campo nome obrigatório.">
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">CPF</label>
                                        <input tabindex="2" type="text" class="form-control" id="cpf" name="cpf" value="" data-validate="required" data-message-required="Campo cpf obrigatório.">
                                    </div>
                                </div>

                            </div>
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Email</label>
                                        <input tabindex="3" type="text" class="form-control" name="email" value="" data-validate="required" data-message-required="Campo email obrigatório." >
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label class="control-label">Data de Nascimento</label>
                                        <input tabindex="4" type="text" class="form-control" id="dataNascimento" name="data_nascimento" value="" data-validate="required" data-message-required="Campo data de Nascimento obrigatório." >
                                    </div>
                                </div>                                
                            </div>
                        <fieldset style="border: 0px solid #ccc; " >
                            <legend>Contato: </legend>
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label">DDD</label>
                                        <input autofocus type="text" class="form-control" id="ddd" name="ddd[]" placeholder="" data-validate="required" data-message-required="Campo nome obrigatório.">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Telefone</label>
                                        <input type="text" class="form-control maskFone" id="telefone" name="telefone[]" value="" data-validate="required" data-message-required="Campo login obrigatório.">
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <button type="button" id="add-campo" class="btn btn-success pull-left" style="margin-top: 30px;">+</button>
                                </div>

                            </div>

                            <div>
                                <table id="itineraries-table" class="table table-bordered table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>DDD</th>
                                            <th>Numero</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>DDD</th>
                                            <th>Numero</th>
                                            <th>Ações</th>
                                        </tr>
                                    </tfoot>

                                    <tbody id="formulario" > 

                                    </tbody>
                                </table>
                            </div>

                            <div class="row">                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input tabindex="5" type="submit" value="cadastrar" class="btn btn-success pull-left" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
    <script type="text/javascript">
            
        $('#add-campo').click(function() {
            var ddd      = $("#ddd").val();
            var telefone = $("#telefone").val();
            if(ddd) { 
                $('#formulario').append('<tr> <td> '+ddd+' <input hidden type="text" class="form-control" name="contato[ddd][]" value="'+ddd+'"  placeholder="" data-validate="required" data-message-required="Campo nome obrigatório."></td> <td> '+telefone+' <input hidden type="text" class="form-control" name="contato[telefone][]" value="'+telefone+'" data-validate="required" data-message-required="Campo nome obrigatório."> </td> <td style="width: 260px" > <button type="submit" class="btn btn-xs btn-danger btnExcluir"> <i class="glyphicon glyphicon-trash"></i> Delete </button> </td> </tr>');

                $(".btnExcluir").bind("click", Excluir);
                $("#ddd").val("");
                $("#telefone").val("");
            }
        });

        function Excluir(){
            var par = $(this).parent().parent(); //tr
            par.remove();
        };

    </script>

@endsection