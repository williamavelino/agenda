@extends('layouts.app')

@section('content')

    <div class="row">
        <div class="col-sm-12">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title"><i class="fa-user"></i> Editando dados de Pessoa</h2>
                </div>

                <div class="panel-body">

                    <form method="POST" action="{{ route('pessoas.update', $pessoa->id) }}" role="form" class="validate">
                        <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="id" id="id" value="{{ $pessoa->id }}">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Nome</label>
                                    <input autofocus tabindex="1" type="text" class="form-control" id="nome" name="nome" placeholder="" data-validate="required" value="{{ $pessoa->nome }}" data-message-required="Campo nome obrigatório.">
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">CPF</label>
                                    <input tabindex="2" type="text" class="form-control" id="cpf" name="cpf" value="{{ $pessoa->formatedCpf() }}" data-validate="required" data-message-required="Campo login obrigatório.">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Email</label>
                                    <input tabindex="3" type="email" class="form-control" name="email" value="{{ $pessoa->email }}" data-validate="required" value="{{ $pessoa->email }}" data-message-required="Campo email obrigatório." >
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label">Data de Nascimento</label>
                                    <input tabindex="4" type="dataNascimento" class="form-control" id="dataNascimento" name="dataNascimento" value="{{ $pessoa->formatedDataNascimento() }}" data-validate="required" data-message-required="Campo Data de Nascimento obrigatório." >
                                </div>
                            </div>                                
                        </div>

                        <fieldset style="border: 0px solid #ccc; " >
                            <legend>Contato: </legend>
                            <div class="row">
                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label class="control-label">DDD</label>
                                        <input autofocus type="text" class="form-control" id="ddd" name="ddd[]" placeholder="" data-validate="required" data-message-required="Campo nome obrigatório.">
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label class="control-label">Telefone</label>
                                        <input type="text" class="form-control maskFone" id="telefone" name="telefone[]" value="" data-validate="required" data-message-required="Campo login obrigatório.">
                                    </div>
                                </div>
                            
                                <div class="form-group">
                                    <button type="button" id="add-campo" class="btn btn-success pull-left" style="margin-top: 30px;">+</button>
                                </div>

                            </div>
</form>
                            <div>
                                <table id="itineraries-table" class="table table-dark table-bordered table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>DDD</th>
                                            <th>Numero</th>
                                            <th>Ações</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>DDD</th>
                                            <th>Numero</th>
                                            <th>Ações</th>
                                        </tr>
                                    </tfoot>

                                    <tbody id="formulario" > 
                                        @foreach($telefones as $telefone)
                                        <tr class="row-{{$telefone->id}}" >
                                            <td>{{ $telefone->formatedDddd() }}</td>
                                            <td>{{ $telefone->numero }}</td>
                                            <td style="width: 260px" class="text-left" >
                                                <a class="btn btn-xs btn-danger" 
                                                onclick="if(confirm('Deseja realmente excluir?')) { deleteTelefone({{$telefone->id}}); } else {return false };" >
                                                <i class="glyphicon glyphicon-remove"></i> Excluir</a>
                                            </td>
                                        </tr>
                                        @endforeach 
                                    </tbody>
                                </table>
                            </div>

                            <div class="row">                                
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input tabindex="5" type="submit" value="Atualizar" class="btn btn-success pull-left" />
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    
                </div>
            </div>
        </div>
    </div>
    
<script type="text/javascript">
            
$('#add-campo').click(function() {
    var ddd      = $("#ddd").val();
    var telefone = $("#telefone").val();
    if(ddd) {         $('#formulario').append('<tr> <td> '+ddd+' <input hidden type="text" class="form-control" name="contato[ddd][]" value="'+ddd+'"  placeholder="" data-validate="required" data-message-required="Campo nome obrigatório."></td> <td> '+telefone+' <input hidden type="text" class="form-control" name="contato[telefone][]" value="'+telefone+'" data-validate="required" data-message-required="Campo nome obrigatório."> </td> <td style="width: 260px" > <button type="submit" class="btn btn-xs btn-danger btnExcluir"> <i class="glyphicon glyphicon-trash"></i> Delete </button> </td> </tr>');

        $(".btnExcluir").bind("click", Excluir);
            var ddd      = $("#ddd").val("");
            var telefone = $("#telefone").val("");
    }
});

function Excluir(){
    var par = $(this).parent().parent(); //tr
    par.remove();
};

function deleteTelefone(id){
    var idTelefone = id;
    $.get("/telefones/"+id+"/delete", 
        { _token: $('#_token').val(), id: idTelefone },
        function(data) {
            $("#formulario").html("");
            listaTelefones($('#id').val());
        });
}

function listaTelefones(id) {
    var idTelefone = id;
    $("#formulario").load("/telefones/"+idTelefone+"/lista");
}

</script>

@endsection