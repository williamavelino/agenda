@extends('layouts.app')

@section('content')

    <h1>Listagem de Pessoas</h1> 


<div class="panel-body">

    <form method="POST" action="{{ URL::to('pessoas/search') }}" role="form" class="validate">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">Nome</label>
                        <input autofocus tabindex="1" type="text" class="form-control" id="nome" name="nome" placeholder="" data-validate="required" value="" data-message-required="Campo nome obrigatório.">
                    </div>
                </div>

                <div class="col-md-3">
                    <div class="form-group">
                        <label class="control-label">CPF</label>
                        <input tabindex="2" type="text" class="form-control" name="cpf" value="" data-validate="required" value="" data-message-required="Campo login obrigatório.">
                    </div>
                </div>
            
                <div class="col-md-2">
                    <div class="form-group">
                        <input tabindex="3" type="submit" value="pesquisar" class="btn btn-success pull-left" />
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <a tabindex="3" href="{{ URL::to('pessoas/create') }}" class="btn btn-primary" >Cadastrar</a>
                    </div>
                </div>

                
            </div>
    </form>

    <table id="itineraries-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Nome</th>                                
                <th>Email</th>
                <th>CPF</th>
                <th>Idade</th>
                <th>Qtd de Telefones</th>
                <th>Ações</th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Nome</th>                                
                <th>Email</th>
                <th>CPF</th>
                <th>Idade</th>
                <th>Qtd de Telefones</th>
                <th>Ações</th>
            </tr>
        </tfoot>

        <tbody>
        @if(count($pessoas))
            @foreach($pessoas as $pessoa)
            <tr>
                <td>{{ $pessoa->nome }}</td>
                <td>{{ $pessoa->email }}</td>
                <td>{{ $pessoa->formatedCpf() }}</td>
                <td>{{ $pessoa->formatedDataNascimento() }}</td>
                <td>{{ $pessoa->telefone()->count() }}</td>
                <td style="width: 160px;" class="text-left" >
                    <a href="/pessoas/{{$pessoa->id}}/edit" class="btn btn-sm btn-success btn-icon" >
                        Editar 
                    </a> 
                    
                    <form style="float: right;" action="{{ URL::to('/pessoas', $pessoa->id) }}" method="POST" >
                        <input type="hidden" name="_method" value="DELETE">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                    </form>
                </td>
            </tr>
            @endforeach 
        @else 
            <tr>
                <td colspan="6" >Nenhum registro encontrado.</td>
            </tr>
        @endif 
        </tbody>
    </table>

</div>
@endsection