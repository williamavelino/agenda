@if(count($telefones))
    @foreach($telefones as $telefone)
    <tr class="row-{{$telefone->id}}" >
        <td>{{ $telefone->formatedDddd() }}</td>
        <td>{{ $telefone->numero }}</td>
        <td style="width: 260px" class="text-left" >
            <a href="#" class="btn btn-xs btn-danger" 
            onclick="if(confirm('Excluir')) { deleteTelefone({{$telefone->id}}); } else {return false };" >
            <i class="glyphicon glyphicon-remove"></i> Excluir</a>                                               
        </td>
    </tr>
    @endforeach  
@else  
    <tr colspan="3" >
        Nenhum telefone cadastrado.
    </tr>
@endif 