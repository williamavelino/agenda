<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return view('welcome');
	$url = "/pessoas";
	return Redirect::intended($url);
});

//Route::get('pessoas/search', 'PessoasController@search');

Route::resource('pessoas', 'PessoasController');
Route::any('pessoas/search', array('uses' => 'PessoasController@search', 'as' => 'pessoas.search'));
Route::post('pessoas/telefone', array('uses' => 'PessoasController@telefone', 'as' => 'pessoas.telefone'));


Route::resource('telefones', 'TelefonesController');
Route::get('telefones/{id}/lista', array('uses' => 'TelefonesController@lista', 'as' => 'telefones.lista'));
Route::get('telefones/{id}/delete', array('uses' => 'TelefonesController@delete',   'as' => 'telefones.delete'));