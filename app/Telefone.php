<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model {

    protected $table = 'telefones';

    public $timestamps = false;

    protected $fillable = [
        'ddd', 'numero', 'idpessoa',
    ];

    public function pessoas()
    {
        return $this->hasMany('App\Pessoa');
    }

    public function formatedDddd()
    {
        if ($this->ddd) {
            return "(".$this->ddd.")";
        }
    }

    public function formatedTelefone()
    {
        //return strlen($this->numero);
        if (strlen($this->numero) == 9) {
            $telefone = $this->Mask("#####-##-##", $this->numero);
        } else {
            $telefone = $this->Mask("####-##-##", $this->numero);
        }

        return $telefone;
    }


    public function formatedDataNascimento()
    {
        if ($this->data_nascimento) {
            return "(".$this->data_nascimento.")";
        } else {
            return "";
        }
    }

    public function setDddAttribute($ddd)
    {

        $ddd = str_replace("(", "", $ddd);
        $this->attributes['ddd'] = str_replace(")", "", $ddd);
    }

    public function setNumeroAttribute($numero)
    {

        
        $this->attributes['numero'] = str_replace("-", "", $numero);
    }

    public static function Mask($mask, $str){

        $str = str_replace(" ","",$str);

        for($i=0;$i<strlen($str);$i++){
            $mask[strpos($mask,"#")] = $str[$i];
        }

        return $mask;

    }

}
