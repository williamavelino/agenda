<?php 

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use App\Http\Controllers\DateTimes;

class Pessoa extends Model {

    protected $table = 'pessoas';

    public $timestamps = false;

    protected $fillable = [
        'nome', 'cpf', 'email', 'data_nascimento',
    ];

    public function formatedCreatedDataNascimento()
    {
        if ($this->dataNascimento) {
            //return Date::parse($this->dataNascimento)->format('d/m/Y');
        }
    } 

    public function setDataNascimentoAttribute($data_nascimento)
    {
        //$this->attributes['dataNascimento'] = Date::parse($data_nascimento)->format('Y-m-d');
        $data_nascimento = implode('-', array_reverse(explode('/', $data_nascimento)));
        $this->attributes['data_nascimento'] = ($data_nascimento);
    }    

    public function setCpfAttribute($cpf)
    {
        $cpf = str_replace(".", "", $cpf);
        $this->attributes['cpf'] = str_replace("-", "", $cpf);
    } 

    public function formatedDataNascimento()
    {
        if ($this->data_nascimento) {
            return Date::parse($this->data_nascimento)->format('d/m/Y');
        } else {
            return "";
        }
    }

    public function idade()
    {
        if ($this->data_nascimento) {
            // Declara a data! :P
            $data = $this->data_nascimento;
           
            // Separa em dia, mês e ano
            list($ano, $mes, $dia) = explode('-', $data);
           
            // Descobre que dia é hoje e retorna a unix timestamp
            $hoje = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
            // Descobre a unix timestamp da data de nascimento do fulano
            $nascimento = mktime( 0, 0, 0, $mes, $dia, $ano);
           
            // Depois apenas fazemos o cálculo já citado :)
            $idade = floor((((($hoje - $nascimento) / 60) / 60) / 24) / 365.25);

            if($idade > 1){
                $complemento = " anos";
            } else {
                $complemento = " ano";
            }
            return $idade . $complemento;
            
        } else {
            return "";
        }
    }    

    public function formatedCpf()
    {

        if (strlen($this->cpf) === 11) {
            return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $this->cpf);
        }

        return $this->cpf;
    }

    public function setCpfAtAttribute($cpf)
    {
        $this->attributes['cpd'] = Carbon::createFromFormat('Y-m-d', $cpf);
    }

    public function telefone()
    {
        return $this->hasOne('App\Telefone', 'idpessoa');
    }

}
