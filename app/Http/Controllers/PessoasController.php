<?php

namespace App\Http\Controllers;

use App\User;
use App\Pessoa;
use App\Telefone;
use Illuminate\Http\Request;

use Jenssegers\Date\Date;

class PessoasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function __construct(Pessoa $pessoa, Telefone $telefone)
    {

        $this->pessoa   = $pessoa;
        $this->telefone = $telefone;
    }

    public function index()
    {

        $pessoas = $this->pessoa->with('telefone')->orderBy('nome', 'asc')->get();
        //dd(count($pessoas[1]->telefone()));
        return view('pessoas.index', compact('pessoas'));
    }

    public function search(Request $request)
    {

        if($request->nome){
            $pessoas = $this->pessoa->where('nome', 'like', '%'.$request->nome.'%')->orderBy('nome', 'asc')->get();
        } else if($request->cpf){
            $cpf = str_replace(".", "", $request->cpf);
            $cpf = str_replace("-", "", $cpf);
            //echo $cpf; die;
            $pessoas = $this->pessoa->where('cpf', 'like', '%'.$cpf.'%')->orderBy('cpf', 'asc')->get();
        } else {
            $pessoas = $this->pessoa->orderBy('cpf', 'asc')->get();
        }

        return view('pessoas.index', compact('pessoas'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        

        return view('pessoas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $pessoa = new Pessoa();

        $pessoa->nome       = $request->input("nome");
        $pessoa->cpf        = $request->input("cpf");
        $pessoa->email      = $request->input("email");
        $pessoa->dataNascimento  = $request->data_nascimento;
        $pessoa->telefones  = 1;

        $pessoa->save();
   
        $contatos = $request->contato;

        $c = 0;

        foreach ($contatos as $k => $v) {
            if(!empty($contatos['ddd'][$c])) {
                $telefone = new Telefone(); 
                $telefone->ddd      = $contatos['ddd'][$c];
                $telefone->numero   = $contatos['telefone'][$c];
                $telefone->idpessoa = $pessoa->id;

                $telefone->save();

                $c++;
            }
        }


        return redirect()->route('pessoas.index')->with('message', 'Cadastro criado com sucesso.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {


        $pessoa = Pessoa::findOrFail($id);

        $telefones = Telefone::where('idpessoa', $id)->get();

        return view('pessoas.edit', compact('pessoa', 'telefones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request, $id)
    {

        $pessoa = $this->pessoa->findOrFail($id);
        
        $pessoa->nome = $request->input("nome");
        $pessoa->cpf  = $request->input("cpf");
        
        $pessoa->dataNascimento  = $request->data_nascimento;
        //dd(Date::parse($request->input("dataNascimento"))->format('Y-m-d'));
        $data = implode('-', array_reverse(explode('/', $request->dataNascimento)));
        //printf($data); die;
        //$request->dataNascimento = date("Y-m-d", strtotime($request->dataNascimento));
        $pessoa->data_nascimento = $data;
        
        $pessoa->email  = $request->input("email");
//dd($pessoa); die;
        $pessoa->save();

        $contatos = @$request->contato;

        $c = 0;
        
        //dd($pessoa); die;

        if($contatos) {
            foreach ($contatos as $k => $v) {
                if(!empty($contatos['ddd'][$c])) {
                    $telefone = new Telefone();
                    $telefone->ddd      = $contatos['ddd'][$c];
                    $telefone->numero   = $contatos['telefone'][$c];
                    $telefone->idpessoa = $id;
                    
                    $c++;
                    $telefone->save();
                }
            }
        }

        return redirect()->route('pessoas.index')->with('message', 'Pessoa atualizado com sucesso.');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pessoa = $this->pessoa->findOrFail($id);

        
        $pessoa->delete();

        $pessoas = $this->pessoa->orderBy('nome', 'asc')->get();

        return redirect()->route('pessoas.index', 'pessoas')->with('message', 'Pessoa deletado com sucesso.');
    }

    public function telefone(Request $request)
    {

        $telefone = new Telefone();

        $telefone->ddd      = $request->input("ddd");
        $telefone->numero   = $request->input("numero");
        $telefone->idpessoa = $request->input("id");
        $telefone->save();
        
        return redirect()->route('pessoas.index')->with('message', 'Telefone adicionado com sucesso.');
    }

}
